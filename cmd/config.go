package main

import (
	"github.com/djacobs24/rest-api/pkg/env"
)

func newConfig() *config {
	return &config{
		env:      env.LookupWithDefault("ENVIRONMENT", "dev"),
		port:     env.LookupIntWithDefault("PORT", 8080),
		certFile: env.LookupWithDefault("CERT_FILE", ""),
		keyFile:  env.LookupWithDefault("KEY_FILE", ""),
	}
}

type config struct {
	env      string
	port     int
	certFile string
	keyFile  string
}
