package main

import (
	goerrs "errors"
	"fmt"
	"os"

	"github.com/djacobs24/rest-api/actions"
	"github.com/djacobs24/rest-api/api"
	"github.com/djacobs24/rest-api/pkg/hash/bcrypt"
	"github.com/djacobs24/rest-api/pkg/log"
	"github.com/djacobs24/rest-api/security"
	"github.com/djacobs24/rest-api/store/mock"
)

func main() {
	if err := run(); err != nil {
		os.Exit(1)
	}
}

func run() error {
	logger, err := log.NewLogger()
	if err != nil {
		return fmt.Errorf("error creating logger: %w", err)
	}

	defer func() {
		_ = logger.Sync()
	}()

	//==============================
	// Get Config
	//==============================
	logger.Info("getting config...")
	cfg := newConfig()

	//==============================
	// Setup Clients
	//==============================
	logger.Info("setting up clients...")

	bcryptClient := bcrypt.NewClient()

	// TODO: Use real stores
	policyStore := mock.PolicyStore{Err: goerrs.New("something happened in the db")}
	sessionStore := mock.SessionStore{Err: goerrs.New("something happened in the db")}
	userStore := mock.UserStore{Err: goerrs.New("something happened in the db")}

	securityManager, err := security.NewManager(policyStore, sessionStore, userStore, bcryptClient)
	if err != nil {
		return fmt.Errorf("error creating security manager: %w", err)
	}

	//==============================
	// Setup Actions
	//==============================
	logger.Info("setting up actions...")

	userActions, err := actions.NewUserActions(sessionStore, userStore, bcryptClient)
	if err != nil {
		return fmt.Errorf("error setting up user actions: %s", err)
	}

	//==============================
	// Setup API
	//==============================
	logger.Info("setting up api...")

	actions := api.Actions{
		User: userActions,
	}

	api, err := api.Setup(cfg.env, actions, securityManager, securityManager)
	if err != nil {
		return fmt.Errorf("error creating api: %w", err)
	}

	if !api.IsReady() {
		panic(fmt.Sprintf("api not ready"))
	}

	logger.Infof("listening and serving on port %d", cfg.port)

	return api.ListenAndServe(cfg.port, cfg.certFile, cfg.keyFile)
}
