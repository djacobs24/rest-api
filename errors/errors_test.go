package errors

import (
	"reflect"
	"testing"
)

func Test_newError(t *testing.T) {
	type args struct {
		typ     errType
		id      errID
		options []ErrOption
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			"no type, no id",
			args{
				typ:     "",
				id:      "",
				options: nil,
			},
			Error{
				typ: TypeUndefined,
				id:  IDUndefined,
				msg: "",
			},
		},
		{
			"bad id",
			args{
				typ:     TypeNotFound,
				id:      "bogus id",
				options: nil,
			},
			Error{
				typ: TypeNotFound,
				id:  IDUndefined,
				msg: "",
			},
		},
		{
			"valid id",
			args{
				typ:     TypePermissionDenied,
				id:      IDInvalidCredentials,
				options: nil,
			},
			Error{
				typ:    TypePermissionDenied,
				id:     IDInvalidCredentials,
				pretty: true,
				msg:    "Invalid username or password",
			},
		},
		{
			"valid id, overwrite message",
			args{
				typ:     TypePermissionDenied,
				id:      IDInvalidCredentials,
				options: []ErrOption{WithMessage("another message")},
			},
			Error{
				typ: TypePermissionDenied,
				id:  IDInvalidCredentials,
				msg: "another message",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewError(tt.args.typ, tt.args.id, tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newError() = %v, want %v", got, tt.want)
			}
		})
	}
}
