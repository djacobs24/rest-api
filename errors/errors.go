// Package errors provides the errors that should be returned to our consumers.
package errors

import (
	"encoding/json"
	"net/http"
)

// NewInvalidArgumentError constructs a new invalid argument error.
func NewInvalidArgumentError(id errID, options ...ErrOption) Error {
	return NewError(TypeInvalidArgument, id, options...)
}

// NewNotFoundError constructs a new not found error.
func NewNotFoundError(id errID, options ...ErrOption) Error {
	return NewError(TypeNotFound, id, options...)
}

// NewAlreadyExistsError constructs a new already exists error.
func NewAlreadyExistsError(id errID, options ...ErrOption) Error {
	return NewError(TypeAlreadyExists, id, options...)
}

// NewPermissionDeniedError constructs a new permission denied error.
func NewPermissionDeniedError(id errID, options ...ErrOption) Error {
	return NewError(TypePermissionDenied, id, options...)
}

// NewUnauthenticatedError constructs a new unauthenticated error.
func NewUnauthenticatedError(id errID, options ...ErrOption) Error {
	return NewError(TypeUnauthenticated, id, options...)
}

// NewUnimplementedError constructs a new unimplemented error.
func NewUnimplementedError(id errID, options ...ErrOption) Error {
	return NewError(TypeUnimplemented, id, options...)
}

// NewInternalError constructs a new internal error.
func NewInternalError(id errID, options ...ErrOption) Error {
	return NewError(TypeInternal, id, options...)
}

func NewError(typ errType, id errID, options ...ErrOption) Error {
	e := &Error{
		typ: typ,
		id:  id,
	}

	idToMsg, ok := errors[typ]
	if !ok {
		e.typ = TypeUndefined
		e.id = IDUndefined
	}

	msg, ok := idToMsg[id]
	if !ok {
		e.id = IDUndefined
	}

	if e.typ != TypeUndefined && e.id != IDUndefined {
		e.pretty = true
	}

	e.msg = msg

	// Apply any options
	for _, opt := range options {
		opt(e)
	}

	return *e
}

// ErrOption is an optional parameter for an error message
type ErrOption func(e *Error)

// WithMessage appends an error message that should NOT be presented to the user
func WithMessage(msg string) ErrOption {
	return func(e *Error) {
		e.msg = msg
		e.pretty = false
	}
}

// WithPrettyMessage appends an error message that can be presented to the user
func WithPrettyMessage(msg string) ErrOption {
	return func(e *Error) {
		e.msg = msg
		e.pretty = true
	}
}

// Error represents all errors in the platform
type Error struct {
	typ    errType
	id     errID
	pretty bool
	msg    string
}

func (e Error) Type() errType {
	return e.typ
}

func (e Error) ID() errID {
	return e.id
}

func (e Error) Pretty() bool {
	return e.pretty
}

func (e Error) Error() string {
	return e.msg
}

// Marshal returns the JSON representation of the error
func (e Error) Marshal() []byte {
	b, _ := json.Marshal(struct {
		Type    errType `json:"type"`
		ID      errID   `json:"id"`
		Pretty  bool    `json:"pretty"`
		Message string  `json:"message"`
	}{
		Type:    e.typ,
		ID:      e.id,
		Pretty:  e.pretty,
		Message: e.msg,
	})

	return b
}

// errType defines all error types.
// Frontend will use this along with the errID to determine
// what to present to the user.
type errType string

const (
	TypeInvalidArgument  errType = "invalid_argument"
	TypeNotFound         errType = "not_found"
	TypeAlreadyExists    errType = "already_exists"
	TypePermissionDenied errType = "permission_denied"
	TypeUnauthenticated  errType = "unauthenticated"
	TypeUnimplemented    errType = "unimplemented"
	TypeInternal         errType = "internal"
	// Reserved for undefined errors
	TypeUndefined errType = "undefined"
)

var TypeToStatusCode = map[errType]int{
	TypeInvalidArgument:  http.StatusBadRequest,
	TypeNotFound:         http.StatusNotFound,
	TypeAlreadyExists:    http.StatusConflict,
	TypePermissionDenied: http.StatusForbidden,
	TypeUnauthenticated:  http.StatusUnauthorized,
	TypeUnimplemented:    http.StatusNotImplemented,
	TypeInternal:         http.StatusInternalServerError,
	TypeUndefined:        http.StatusInternalServerError,
}

// errID defines all error ids.
// Frontend will use this along with the errType to determine
// what to present to the user.
type errID string

const (
	IDUndefined           errID = "undefined"
	IDInvalidRequest      errID = "invalid_request"
	IDInternal            errID = "internal"
	IDDatabase            errID = "database"
	IDPermissionDenied    errID = "permission_denied"
	IDInvalidSession      errID = "invalid_session"
	IDSessionExpired      errID = "expired_session"
	IDNoSessionCookie     errID = "no_session_cookie"
	IDInvalidCredentials  errID = "invalid_credentials"
	IDMustBeSelf          errID = "must_be_self"
	IDMustNotBeSelf       errID = "must_not_be_self"
	IDNotifyNotConfigured errID = "notify_not_configured"
)

// errors combines errTypes with errIDs along with a default message
var errors = map[errType]map[errID]string{
	TypeInvalidArgument: {
		IDInvalidRequest: "There was an error with the request",
	},
	TypeNotFound:      {},
	TypeAlreadyExists: {},
	TypePermissionDenied: {
		IDInvalidCredentials: "Invalid username or password",
		IDPermissionDenied:   "You do not have permission to do that",
		IDMustBeSelf:         "This can only be applied by the user making the request",
	},
	TypeUnauthenticated: {
		IDInvalidSession:  "Invalid session",
		IDSessionExpired:  "Your session has expired",
		IDNoSessionCookie: "You must be logged in to do that",
	},
	TypeUnimplemented: {
		IDNotifyNotConfigured: "Notifications not configured",
	},
	TypeInternal: {
		IDInternal: "Something went wrong",
		IDDatabase: "Database error",
	},
}
