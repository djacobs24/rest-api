package store

import (
	"fmt"
)

// ErrNotFound is there error used when something was not found
// in the database.
type ErrNotFound struct {
	Obj string
}

func (e ErrNotFound) Error() string {
	return fmt.Sprintf("%s not found", e.Obj)
}

// ErrAlreadyExists is the error used when something already exists
// in the database.
type ErrAlreadyExists struct {
	Obj   string
	Field string
}

func (e ErrAlreadyExists) Error() string {
	return fmt.Sprintf("%s already exists with that %s", e.Obj, e.Field)
}
