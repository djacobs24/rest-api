package store

import (
	"context"

	"github.com/djacobs24/rest-api/model"
)

// Policy holds the methods used to access the policy store.
type Policy interface {
	FindPoliciesForSubjectOnResource(ctx context.Context, subject model.PolicySubject, resource model.PolicyResource) ([]model.Policy, error)
}

// Session holds the methods used to access the session store.
type Session interface {
	FindByToken(ctx context.Context, token string) (model.Session, error)
	Create(ctx context.Context, session model.Session) error
	DeleteByUserID(ctx context.Context, userID string) error
}

// User holds the methods used to access the session store.
type User interface {
	FindByUsername(ctx context.Context, username string) (model.User, error)
	Create(ctx context.Context, user model.User) error
}
