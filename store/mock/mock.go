package mock

import (
	"context"

	"github.com/djacobs24/rest-api/model"
	"github.com/djacobs24/rest-api/store"
)

var _ store.Policy = (*PolicyStore)(nil)

// PolicyStore ...
type PolicyStore struct {
	Policies []model.Policy
	Err      error
}

// FindPoliciesForSubjectOnResource ...
func (s PolicyStore) FindPoliciesForSubjectOnResource(ctx context.Context, subject model.PolicySubject, resource model.PolicyResource) ([]model.Policy, error) {
	return s.Policies, s.Err
}

var _ store.Session = (*SessionStore)(nil)

// SessionStore ...
type SessionStore struct {
	Session model.Session
	Err     error
}

// FindByToken ...
func (s SessionStore) FindByToken(ctx context.Context, token string) (model.Session, error) {
	return s.Session, s.Err
}

// Create ...
func (s SessionStore) Create(ctx context.Context, session model.Session) error {
	return s.Err
}

// DeleteByUserID ...
func (s SessionStore) DeleteByUserID(ctx context.Context, userID string) error {
	return s.Err
}

var _ store.User = (*UserStore)(nil)

// UserStore ...
type UserStore struct {
	User model.User
	Err  error
}

// FindByUsername ...
func (s UserStore) FindByUsername(ctx context.Context, username string) (model.User, error) {
	return s.User, s.Err
}

// Create ...
func (s UserStore) Create(ctx context.Context, user model.User) error {
	return s.Err
}
