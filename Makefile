REGISTRY=registry.gitlab.com/djacobs24/rest-api
TAG = $(shell git describe --always)
UNIT_TEST_PKGS = $(shell go list ./... | grep -v /test)
E2E_TEST_PKGS = $(shell go list ./test/...)

#==================================================
# Lint
#==================================================

.PHONY: lint
lint:
	golangci-lint run

.PHONY: tidy
tidy:
	go mod tidy

#==================================================
# Build
#==================================================

.PHONY: build
build:
	CGO_ENABLED=0 GOOS=linux go build -v -ldflags="-s -w" -o ./app ./cmd

.PHONY: docker-build
docker-build: build
	docker build -t $(REGISTRY):$(TAG) .

.PHONY: docker-push
docker-push: build docker-build
	docker push $(REGISTRY):$(TAG)

#==================================================
# Run
#==================================================

.PHONY: up
up:
	go run ./cmd

#==================================================
# Test
#==================================================

.PHONY: test
test:
	go test -cover $(UNIT_TEST_PKGS)

.PHONY: test-e2e
test-e2e:
	go test -v -failfast $(E2E_TEST_PKGS)

#==================================================
# Docs
#==================================================

.PHONY: gen-docs
gen-docs:
	docker run --rm -i yousan/swagger-yaml-to-html < ./api/docs/openapi.yaml > ./api/docs/openapi.html
