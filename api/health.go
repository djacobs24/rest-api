package api

import (
	"net/http"

	"github.com/djacobs24/rest-api/errors"
)

// healthCheck is the handler for checking the health of the application.
func (a *API) healthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !a.ready {
			writeError(w, errors.NewInternalError(errors.IDInternal, errors.WithMessage("application not in ready state")))

			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
