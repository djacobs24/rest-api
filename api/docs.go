package api

import (
	_ "embed"
	"net/http"
)

//go:embed docs/openapi.html
var apiDocs []byte

// getDocumentation renders the API documentation.
func (a *API) getDocumentation() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		writeHTMLResponse(w, apiDocs)
	}
}
