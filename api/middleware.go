package api

import (
	"fmt"
	"net/http"

	"github.com/djacobs24/rest-api/errors"
	"github.com/djacobs24/rest-api/model"
)

// authenticate is middleware used to check the session on the request
func (a *API) authenticate(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		sessToken := sessionTokenFromRequest(r)
		if sessToken == "" {
			writeError(w, errors.NewUnauthenticatedError(errors.IDNoSessionCookie))

			return
		}

		a.log.Debugf("authenticating with session '%s'", sessToken)

		sess, err := a.authenticator.AuthenticateWithToken(r.Context(), sessToken)
		if err != nil {
			writeError(w, err)

			return
		}

		// Add the session value to the context
		ctx := ctxWithSession(r.Context(), sess)

		// Call the next handler with the session value on the context
		next(w, r.WithContext(ctx))
	}
}

// authorize is middleware used to make sure the subject has permission to the resource
func (a *API) authorize(next http.HandlerFunc, resource model.PolicyResource, action model.PolicyAction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		subject := model.PolicySubject{
			ID:   userIDFromCtx(r.Context()),
			Type: model.UserSubject,
		}

		if err := a.authorizer.Authorize(r.Context(), subject, resource, action); err != nil {
			writeError(w, err)

			return
		}

		// Call the wrapped handler
		next(w, r)
	}
}

// mustBeSelf is middleware used to verify the subject making the request is the
// same as the subject referenced in the request
// This must be called after Authenticate since Authenticate puts the user id on the context
func (a *API) mustBeSelf(next http.HandlerFunc, subjectIDPathVar string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		a.log.Debug("authorizing user is acting on self...")

		// Compare the user id in the context (from the session) and
		// the user id in the route and makes sure they are the same
		if err := isSelf(r, subjectIDPathVar); err != nil {
			a.log.Error(err)
			writeError(w, err)

			return
		}

		// Call the wrapped handler
		next(w, r)
	}
}

// authorizeUnlessSelf is middleware used to check if the subject making the request
// is the same as the subject referenced in the request OR they have permission to
// the resource.
func (a *API) authorizeUnlessSelf(next http.HandlerFunc, subjectIDPathVar string, resource model.PolicyResource, action model.PolicyAction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		a.log.Debug("authorizing if not self...")

		if err := isSelf(r, subjectIDPathVar); err == nil {
			// The user in the path is the one making the request
			next(w, r)

			return
		}

		// The user is making a request for another user, authorize them
		a.authorize(next, resource, action)(w, r)
	}
}

// isSelf compares the user id in the context (from the session) and
// the user id in the route and makes sure they are the same
func isSelf(r *http.Request, subjectIDPathVar string) error {
	userIDFromContext := userIDFromCtx(r.Context())
	userIDFromPath := pathParam(r, subjectIDPathVar)
	if userIDFromContext != userIDFromPath {
		return errors.NewPermissionDeniedError(errors.IDMustBeSelf, errors.WithMessage(fmt.Sprintf("user with id %s trying to act on user with id %s", userIDFromContext, userIDFromPath)))
	}

	return nil
}

// logRequest is middleware used to add logging information about the request
func (a *API) logRequest(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		a.log.Debugf("~ %s: %s", r.Method, r.URL)

		// Call the wrapped handler
		next(w, r)
	}
}
