package api

import (
	"net/http"

	"github.com/djacobs24/rest-api/actions"
)

// login is the handler for logging a user into the app.
func (a *API) login() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &actions.LoginRequest{}
		if err := decodeAndValidateRequest(r, req); err != nil {
			writeError(w, err)

			return
		}

		resp, err := a.actions.User.Login(r.Context(), req)
		if err != nil {
			writeError(w, err)

			return
		}

		setSessionCookie(w, resp.SessionToken, resp.SessionTTL)

		writeJSONResponse(w, resp, err)
	}
}

// logout is the handler for logging a user out of the app.
func (a *API) logout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &actions.LogoutRequest{
			UserID: userIDFromCtx(r.Context()),
		}

		if err := req.Validate(); err != nil {
			writeJSONResponse(w, actions.LogoutResponse{}, nil)

			return
		}

		resp, err := a.actions.User.Logout(r.Context(), req)
		if err != nil {
			a.log.Errorf(err.Error())
		}

		clearSessionCookie(w)

		writeJSONResponse(w, resp, nil)
	}
}

// signup is the handler for signing up for an account in the app.
func (a *API) signup() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &actions.SignUpRequest{}
		if err := decodeAndValidateRequest(r, req); err != nil {
			writeError(w, err)

			return
		}

		resp, err := a.actions.User.SignUp(r.Context(), req)
		writeJSONResponse(w, resp, err)
	}
}
