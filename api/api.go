package api

import (
	"context"
	"encoding/json"
	goerrs "errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/djacobs24/rest-api/errors"
	"github.com/djacobs24/rest-api/model"
	"github.com/djacobs24/rest-api/pkg/log"
	"github.com/djacobs24/rest-api/security"
	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"github.com/djacobs24/rest-api/actions"
)

// Cookies
const (
	sessionCookieName = "session_token"
)

// Query Parameters
const (
	pageParam    = "page"
	perPageParam = "per_page"
	sortParam    = "sort"
	searchParam  = "search"
)

const (
	contentTypeKey  = "Content-Type"
	jsonContentType = "application/json"
	htmlContentType = "text/html; charset=utf-8"
)

const (
	devEnv = "dev"
)

// Setup constructs a new API, sets up the routes, and marks it ready
func Setup(env string, actions Actions, authenticator security.Authenticator, authorizer security.Authorizer) (*API, error) {
	logger, err := log.NewLogger(log.WithName("api"))
	if err != nil {
		return nil, fmt.Errorf("error creating logger: %s", err)
	}

	api := &API{
		env:           env,
		actions:       actions,
		authenticator: authenticator,
		authorizer:    authorizer,
		router:        mux.NewRouter(),
		log:           logger,
	}

	api.setupRoutes()
	api.markReady()

	return api, nil
}

// setupRoutes sets up all routes our API will listen and serve on.
func (a *API) setupRoutes() {
	routes := a.makeRoutes()

	for _, route := range routes {
		if route.authorize {
			// Wrap the handler with the authorize handler
			route.handlerFunc = a.authorize(route.handlerFunc, route.resource, route.action)
		}
		if route.mustBeSelf {
			// Wrap the handler with the must be self handler
			route.handlerFunc = a.mustBeSelf(route.handlerFunc, route.subjectIDPathVar)
		}
		if route.authorizeUnlessSelf {
			// Wrap the handler with the authorize unless self handler
			route.handlerFunc = a.authorizeUnlessSelf(route.handlerFunc, route.subjectIDPathVar, route.resource, route.action)
		}
		if route.authenticate {
			// Wrap the handler with the authenticate handler
			route.handlerFunc = a.authenticate(route.handlerFunc)
		}
		if a.env == devEnv {
			// Wrap the handler with a logger
			route.handlerFunc = a.logRequest(route.handlerFunc)
		}

		a.router.HandleFunc(route.path, route.handlerFunc).Methods(route.methodType)
	}
}

// markReady puts the API in a ready state
func (a *API) markReady() {
	a.ready = true
}

// IsReady returns the state of the API
func (a *API) IsReady() bool {
	return a.ready
}

// ListenAndServe listens and serves incoming requests
func (a *API) ListenAndServe(port int, certFile, keyFile string) error {
	if certFile == "" || keyFile == "" {
		return http.ListenAndServe(makeAddr(port), a.router)
	}

	if err := http.ListenAndServeTLS(makeAddr(port), certFile, keyFile, a.router); err != nil {
		a.ready = false

		return err
	}

	return nil
}

func makeAddr(port int) string {
	return fmt.Sprintf(":%d", port)
}

// Actions holds all of the actions called by the API
type Actions struct {
	User *actions.UserActions
}

// API holds the dependencies for all API handlers
type API struct {
	ready         bool
	env           string
	actions       Actions
	authenticator security.Authenticator
	authorizer    security.Authorizer
	router        *mux.Router
	log           *zap.SugaredLogger
}

//==================================================
// Route Helpers
//==================================================

type routeOption func(r *route)

// withoutAuthentication turns authentication off for a route.
// Uses the Authentication middleware
func withoutAuthentication() routeOption {
	return func(r *route) {
		r.authenticate = false
	}
}

// withAuthorization turns authorization on for a route.
// Uses the Authorize middleware
func withAuthorization(resource model.PolicyResource, action model.PolicyAction) routeOption {
	return func(r *route) {
		r.authorize = true
		r.resource = resource
		r.action = action
	}
}

// withAuthorizationUnlessSelf turns authorization on for a route if the
// subject id in the route does not match the subject making the request.
// Uses the AuthorizeUnlessSelf middleware.
func withAuthorizationUnlessSelf(subjectIDPathVar string, resource model.PolicyResource, action model.PolicyAction) routeOption {
	return func(r *route) {
		r.authorizeUnlessSelf = true
		r.subjectIDPathVar = subjectIDPathVar
		r.resource = resource
		r.action = action
	}
}

func mustBeSelf(subjectIDPathVar string) routeOption { // nolint:unparam
	return func(r *route) {
		r.mustBeSelf = true
		r.subjectIDPathVar = subjectIDPathVar
	}
}

// newRoute constructs a new route.
func newRoute(path string, methodType string, handlerFunc http.HandlerFunc, options ...routeOption) *route {
	r := &route{
		path:         path,
		methodType:   methodType,
		handlerFunc:  handlerFunc,
		authenticate: true, // Authenticate by default
	}

	for _, opt := range options {
		opt(r)
	}

	return r
}

// route holds all things specific to any given route.
type route struct {
	path                string
	methodType          string
	handlerFunc         http.HandlerFunc
	authenticate        bool
	authorize           bool
	authorizeUnlessSelf bool
	resource            model.PolicyResource
	action              model.PolicyAction
	mustBeSelf          bool   // The subject making the request must match the id in the path
	subjectIDPathVar    string // Used for getting the subject's id from the path
}

//==================================================
// Request Helpers
//==================================================

// RequestValidator provides a method for validating incoming requests.
type RequestValidator interface {
	Validate() error
}

// decodeAndValidateRequest attempts to decode a JSON request and validate it.
func decodeAndValidateRequest(r *http.Request, req RequestValidator) error {
	if r.Body == nil {
		return req.Validate()
	}

	switch r.Header.Get(contentTypeKey) {
	case jsonContentType:
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			return errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithMessage(err.Error()))
		}
		defer r.Body.Close()
	}

	return req.Validate()
}

// pathParam uses mux to get the value of a route parameter
func pathParam(r *http.Request, param string) string {
	return mux.Vars(r)[param]
}

// queryParam returns the query parameter from the path
func queryParam(r *http.Request, param string) string {
	return r.URL.Query().Get(param)
}

// queryMetaFromRequest returns metadata in query params that is used for querying
//
// What we expect: www.example.com?page=3&per_page=30&sort=name,updated_at&search=david
// page: 3
// per_page: 30
// search: david
// sort: name
func queryMetaFromRequest(r *http.Request) model.Meta {
	page, _ := strconv.Atoi(queryParam(r, pageParam))
	perPage, _ := strconv.Atoi(queryParam(r, perPageParam))
	search := queryParam(r, searchParam)
	sort := queryParam(r, sortParam)

	sorts := []string{}
	if sort != "" {
		splitSort := strings.Split(sort, ",")
		for _, s := range splitSort {
			sorts = append(sorts, strings.TrimSpace(s))
		}
	}

	return model.NewMeta(
		model.WithCurrentPage(page),
		model.WithItemsPerPage(perPage),
		model.WithSearchTerm(search),
		model.WithSortBy(sorts),
	)
}

//==================================================
// Response Helpers
//==================================================

// writeJSONResponse is a helper for writing JSON HTTP responses.
func writeJSONResponse(w http.ResponseWriter, resp interface{}, err error) {
	if err != nil {
		writeError(w, err)

		return
	}

	body, err := json.Marshal(resp)
	if err != nil {
		err = errors.NewInternalError(errors.IDInvalidRequest, errors.WithMessage(fmt.Sprintf("Error decoding request body: %s", err)))
		writeError(w, err)

		return
	}

	w.Header().Set(contentTypeKey, jsonContentType)
	w.WriteHeader(http.StatusOK)

	_, err = w.Write(body)
	if err != nil {
		err = errors.NewInternalError(errors.IDInvalidRequest, errors.WithMessage(fmt.Sprintf("Error writing response: %s", err)))
		writeError(w, err)
	}
}

// writeHTMLResponse is a helper for writing HTTP responses.
func writeHTMLResponse(w http.ResponseWriter, html []byte) {
	w.Header().Set(contentTypeKey, htmlContentType)
	w.WriteHeader(http.StatusOK)

	_, err := w.Write(html)
	if err != nil {
		err = errors.NewInternalError(errors.IDInvalidRequest, errors.WithMessage(fmt.Sprintf("Error writing response: %s", err)))
		writeError(w, err)
	}
}

// writeError is a helper for writing HTTP errors. It constructs a default error,
// tries to cast the error passed in into an errors.Error, maps the errors type
// to a status code, sets the appropriate headers, and writes it.
// NOTE: This also clears the session cookie if the status is unauthorized.
func writeError(w http.ResponseWriter, err error) {
	var e errors.Error

	if !goerrs.As(err, &e) {
		e = errors.NewError(
			errors.TypeUndefined,
			errors.IDUndefined,
			errors.WithMessage(err.Error()),
		)
	}

	statusCode, ok := errors.TypeToStatusCode[e.Type()]
	if !ok {
		statusCode = http.StatusInternalServerError
	}

	if statusCode == http.StatusUnauthorized {
		clearSessionCookie(w)
	}

	w.Header().Set(contentTypeKey, jsonContentType)
	w.WriteHeader(statusCode)
	_, _ = w.Write(e.Marshal())
}

//==================================================
// Cookie Helpers
//==================================================

// sessionTokenFromRequest gets the session token from the cookie in the request.
func sessionTokenFromRequest(r *http.Request) string {
	cookie, err := r.Cookie(sessionCookieName)
	if err != nil {
		return ""
	}

	return cookie.Value
}

// setSessionCookie sets the session cookie.
func setSessionCookie(w http.ResponseWriter, token string, sessionExpiry int64) {
	http.SetCookie(w, &http.Cookie{
		Name:     sessionCookieName,
		HttpOnly: true,
		Path:     "/",
		Value:    token,
		MaxAge:   int(sessionExpiry),
	})
}

// clearSessionCookie clears the session cookie.
func clearSessionCookie(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:     sessionCookieName,
		MaxAge:   -1,
		HttpOnly: true,
		Path:     "/",
	})
}

//==================================================
// Context Helpers
//==================================================

// ctxKey is the type used for all context keys
type ctxKey string

const (
	// sessionCtxKey is the key for a user's session on a context
	sessionCtxKey ctxKey = "session"
)

// ctxWithSession sets a session on a context
func ctxWithSession(ctx context.Context, s model.Session) context.Context {
	return context.WithValue(ctx, sessionCtxKey, s)
}

// userIDFromCtx gets a user id from a context
func userIDFromCtx(ctx context.Context) string {
	s := sessionFromCtx(ctx)
	if s == nil {
		return ""
	}

	return s.SubjectID()
}

// sessionFromCtx gets a session from a context
func sessionFromCtx(ctx context.Context) *model.Session {
	s, ok := ctx.Value(sessionCtxKey).(model.Session)
	if !ok {
		return nil
	}

	return &s
}
