package api

import (
	"net/http"
)

func (a *API) makeRoutes() []*route {
	return []*route{
		// Documentation
		newRoute("/docs", http.MethodGet, a.getDocumentation(),
			withoutAuthentication(),
		),

		// Health
		newRoute("/health", http.MethodGet, a.healthCheck(),
			withoutAuthentication(),
		),

		// User
		newRoute("/login", http.MethodPost, a.login(),
			withoutAuthentication(),
		),
		newRoute("/logout", http.MethodPost, a.logout()),
		newRoute("/signup", http.MethodPost, a.signup(),
			withoutAuthentication(),
		),
	}
}
