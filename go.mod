module github.com/djacobs24/rest-api

go 1.16

require (
	github.com/djacobs24/validate v0.0.0-20210511194009-2030cd9ba447
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20190510104115-cbcb75029529
//github.com/djacobs24/validate @v0.0.0-20210506002533-d1cded936421
)
