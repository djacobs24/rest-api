FROM alpine AS build
WORKDIR /workspace
EXPOSE 8080
RUN apk --no-cache add ca-certificates && update-ca-certificates

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY app ./
ENTRYPOINT ["./app"]
