package security

import (
	"context"

	"github.com/djacobs24/rest-api/model"
)

// Authenticator provides methods to determine if a
// subject is who they say they are.
type Authenticator interface {
	AuthenticateWithToken(ctx context.Context, token string) (model.Session, error)
	AuthenticateWithCredentials(ctx context.Context, username, password string) error
}

// Authorizer provides methods to determine if a subject
// has access to a particular resource.
type Authorizer interface {
	Authorize(ctx context.Context, subject model.PolicySubject, resource model.PolicyResource, action model.PolicyAction) error
}
