package security

import (
	"context"
	goerrs "errors"
	"fmt"

	"github.com/djacobs24/rest-api/errors"
	"github.com/djacobs24/rest-api/model"
	"github.com/djacobs24/rest-api/pkg/hash"
	"github.com/djacobs24/rest-api/pkg/log"
	"github.com/djacobs24/rest-api/store"
	"go.uber.org/zap"
)

// NewManager constructs a new Manager.
func NewManager(policyStore store.Policy, sessionStore store.Session, userStore store.User, hasher hash.Hasher) (*Manager, error) {
	logger, err := log.NewLogger(log.WithName("security_manager"))
	if err != nil {
		return nil, fmt.Errorf("error creating logger: %s", err)
	}

	return &Manager{
		policyStore:  policyStore,
		sessionStore: sessionStore,
		userStore:    userStore,
		hasher:       hasher,
		log:          logger,
	}, nil
}

// Manager holds the dependencies needed to authenticate and authorize users.
type Manager struct {
	policyStore  store.Policy
	sessionStore store.Session
	userStore    store.User
	hasher       hash.Hasher
	log          *zap.SugaredLogger
}

// AuthenticateWithToken finds a session in the database and checks
// if it is valid.
func (m *Manager) AuthenticateWithToken(ctx context.Context, token string) (model.Session, error) {
	m.log.Debugf("authenticating with token %s...", token)

	sess, err := m.sessionStore.FindByToken(ctx, token)
	if err != nil {
		if goerrs.As(err, &store.ErrNotFound{}) {
			m.log.Debug("session not found")
			return model.Session{}, errors.NewUnauthenticatedError(errors.IDInvalidSession)
		}

		m.log.Debugf("error getting session: %s", err)
		return model.Session{}, errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error getting session: %s", err)))
	}

	if sess.IsExpired() {
		m.log.Debug("session expired")
		return model.Session{}, errors.NewUnauthenticatedError(errors.IDSessionExpired)
	}

	return sess, nil
}

// AuthenticateWithCredentials finds a user in the database and checks
// if their credentials are valid.
func (m *Manager) AuthenticateWithCredentials(ctx context.Context, username, password string) error {
	m.log.Debugf("authenticating %s...", username)

	user, err := m.userStore.FindByUsername(ctx, username)
	if err != nil {
		if goerrs.As(err, &store.ErrNotFound{}) {
			m.log.Debug("user not found")
			return errors.NewUnauthenticatedError(errors.IDInvalidCredentials)
		}

		m.log.Debugf("error getting user: %s", err)
		return errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error getting session: %s", err)))
	}

	if err := m.hasher.Compare(user.PasswordHash(), password); err != nil {
		m.log.Infof("invalid login try by %s", username)
		return errors.NewUnauthenticatedError(errors.IDInvalidCredentials)
	}

	return nil
}

// Authorize returns nil if the subject can take action on the resource.
func (m *Manager) Authorize(ctx context.Context, subject model.PolicySubject, resource model.PolicyResource, action model.PolicyAction) error {
	m.log.Debugf("authorizing subject with id %s can %s resource with id %s...", subject.ID, resource.ID, action)

	policies, err := m.policyStore.FindPoliciesForSubjectOnResource(ctx, subject, resource)
	if err != nil {
		m.log.Debugf("error getting policies: %s", err)
		return errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error getting policies: %s", err)))
	}

	for _, p := range policies {
		if p.Can(action) {
			return nil
		}
	}

	return errors.NewPermissionDeniedError(errors.IDPermissionDenied, errors.WithPrettyMessage(fmt.Sprintf("You cannot %s that %s", action, resource.Type)))
}
