package test

import (
	"net/http"
	"testing"
)

func Test_Login_DoesNotRequireAuthentication(t *testing.T) {
	session := ""
	httpResp, err := httpPOST("/login", nil, session)
	if err != nil {
		t.Error(err)
	}

	if httpResp.StatusCode != http.StatusBadRequest {
		t.Errorf("expected: %d, got: %d", httpResp.StatusCode, http.StatusBadRequest)
	}
}
