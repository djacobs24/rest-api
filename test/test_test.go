package test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

const (
	urlScheme      = "http"
	urlDomain      = "localhost:8080"
	urlBasePath    = "/api"
	contentTypeKey = "Content-Type"
	sessionCookie  = "session"
)

var (
	jsonContent = "application/json"
)

func TestMain(m *testing.M) {
	// Exit with test exit code
	os.Exit(m.Run())
}

//======================================
// Request helpers
//======================================

func formFileCall(route string, key, filePath, sessionID string) (*http.Response, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	defer writer.Close()

	file, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("error opening %s: %w", filePath, err)
	}
	defer file.Close()

	part, err := writer.CreateFormFile(key, filepath.Base(file.Name()))
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(part, file)
	if err != nil {
		return nil, err
	}

	contentType := writer.FormDataContentType()

	return callAPI(route, http.MethodPost, body, &contentType, sessionID)
}

// httpPOST makes an HTTP POST request to our API using the provided route, body, and session ID
func httpPOST(route string, body interface{}, sessionID string) (*http.Response, error) {
	var b bytes.Buffer
	if body != nil {
		err := json.NewEncoder(&b).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	return callAPI(route, http.MethodPost, &b, &jsonContent, sessionID)
}

// httpPUT makes an HTTP PUT request to our API using the provided route, body, and session ID
func httpPUT(route string, body interface{}, sessionID string) (*http.Response, error) {
	var b bytes.Buffer
	if body != nil {
		err := json.NewEncoder(&b).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	return callAPI(route, http.MethodPut, &b, &jsonContent, sessionID)
}

// httpDELETE makes an HTTP DELETE request to our API using the provided route and session ID
func httpDELETE(route, sessionID string) (*http.Response, error) {
	return callAPI(route, http.MethodDelete, nil, nil, sessionID)
}

// httpGET makes an HTTP GET request to our API using the provided route and session ID
func httpGET(route string, sessionID string) (*http.Response, error) {
	return callAPI(route, http.MethodGet, nil, nil, sessionID)
}

// callAPI makes an HTTP request and calls our API
func callAPI(route, methodType string, body io.Reader, contentType *string, sessionID string) (*http.Response, error) {
	httpRequest, err := makeHTTPRequest(route, methodType, body, contentType, sessionID)
	if err != nil {
		return nil, fmt.Errorf("error making http request: %w", err)
	}

	log.Print(fmt.Sprintf("~ %s: %s", methodType, route))

	client := http.Client{}
	resp, err := client.Do(httpRequest)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// makeHTTPRequest constructs an HTTP request
func makeHTTPRequest(route, methodType string, body io.Reader, contentType *string, sessionID string) (*http.Request, error) {
	req, err := http.NewRequestWithContext(context.Background(), methodType, makeURL(route), body)
	if err != nil {
		return nil, err
	}

	if contentType != nil {
		req.Header.Set(contentTypeKey, *contentType)
	}

	if sessionID == "" {
		return req, nil
	}

	req.AddCookie(&http.Cookie{
		Name:     sessionCookie,
		Value:    sessionID,
		Path:     "/",
		MaxAge:   int(time.Now().Unix() + 86400),
		HttpOnly: true,
	})

	return req, nil
}

// makeURL constructs the URL for the request
func makeURL(route string) string {
	// Prefix route with / if not provided
	if !strings.HasPrefix(route, "/") {
		route = "/" + route
	}

	return fmt.Sprintf("%s://%s%s%s", urlScheme, urlDomain, urlBasePath, route)
}

// getSessionIDFromRequest grabs the session ID from the session cookie
func getSessionIDFromRequest(r *http.Request) string {
	c, err := r.Cookie(sessionCookie)
	if err != nil {
		return ""
	}

	return c.Value
}
