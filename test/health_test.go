package test

import (
	"net/http"
	"testing"
)

func Test_HealthCheck_DoesNotRequireAuthentication(t *testing.T) {
	session := ""
	httpResp, err := httpGET("/health", session)
	if err != nil {
		t.Error(err)
	}

	if httpResp.StatusCode != http.StatusOK {
		t.Errorf("expected: %d, got: %d", httpResp.StatusCode, http.StatusOK)
	}
}
