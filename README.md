# REST API
My _framework_ for how I'm building REST API's these days.

```mermaid
graph TD

%% Definitions
client((Client))
api[REST API]
authentication[Authentication]
authorization[Authorization]
actions[Actions]
store[(Store)]
pkg1([3rd])
pkg2([Party])
pkg3([Deps])

%% Subgraphs
subgraph Security Manager
authentication
authorization
end

%% Flow
client -.-> api
api --> |middleware| authentication
api --> |middleware| authorization
api --> actions
actions --> |Interface| store
actions --> |Interface| pkg1
actions --> |Interface| pkg2
actions --> |Interface| pkg3

%% Links
click api "https://gitlab.com/djacobs24/rest-api/-/tree/main/api"
click authentication "https://gitlab.com/djacobs24/rest-api/-/blob/main/security/security.go"
click authorization "https://gitlab.com/djacobs24/rest-api/-/blob/main/security/security.go"
click actions "https://gitlab.com/djacobs24/rest-api/-/tree/main/actions"
click store "https://gitlab.com/djacobs24/rest-api/-/blob/main/store/store.go"
click pkg1 "https://gitlab.com/djacobs24/rest-api/-/tree/main/pkg"
click pkg2 "https://gitlab.com/djacobs24/rest-api/-/tree/main/pkg"
click pkg3 "https://gitlab.com/djacobs24/rest-api/-/tree/main/pkg"
```
