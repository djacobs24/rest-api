package actions

import (
	"context"
	goerrs "errors"
	"fmt"
	"testing"

	"github.com/djacobs24/rest-api/errors"
	"github.com/djacobs24/rest-api/model"
	"github.com/djacobs24/rest-api/pkg/hash"
	hashMock "github.com/djacobs24/rest-api/pkg/hash/mock"
	"github.com/djacobs24/rest-api/pkg/log"
	"github.com/djacobs24/rest-api/store"
	storeMock "github.com/djacobs24/rest-api/store/mock"
	"go.uber.org/zap"
)

func TestUserActions_Login(t *testing.T) {
	logger, err := log.NewLogger(log.WithName("user_actions"))
	if err != nil {
		t.Errorf("error creating logger: %s", err)
		return
	}

	type fields struct {
		sessionStore store.Session
		userStore    store.User
		hasher       hash.Hasher
		log          *zap.SugaredLogger
	}
	type args struct {
		ctx context.Context
		req *LoginRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		err     error
	}{
		{
			name: "username is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LoginRequest{
					Username: "",
					Password: "password123",
				},
			},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Username is a required field")),
		},
		{
			name: "password is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LoginRequest{
					Username: "david",
					Password: "",
				},
			},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Password is a required field")),
		},
		{
			name: "user not found returns invalid credentials",
			fields: fields{
				sessionStore: nil,
				userStore:    storeMock.UserStore{Err: store.ErrNotFound{Obj: "User"}},
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LoginRequest{
					Username: "david",
					Password: "password",
				},
			},
			wantErr: true,
			err:     errors.NewUnauthenticatedError(errors.IDInvalidCredentials),
		},
		{
			name: "invalid credentials",
			fields: fields{
				sessionStore: nil,
				userStore:    storeMock.UserStore{User: model.NewUser("David", "david", "diffpass")},
				hasher:       hashMock.Hasher{Err: goerrs.New("invalid hash")},
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LoginRequest{
					Username: "david",
					Password: "password",
				},
			},
			wantErr: true,
			err:     errors.NewUnauthenticatedError(errors.IDInvalidCredentials),
		},
		{
			name: "valid credentials",
			fields: fields{
				sessionStore: storeMock.SessionStore{Session: model.NewSession("123")},
				userStore:    storeMock.UserStore{User: model.NewUser("David", "david", "hashedpassword")},
				hasher:       hashMock.Hasher{Hash: "hashedpassword"},
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LoginRequest{
					Username: "david",
					Password: "password",
				},
			},
			wantErr: false,
			err:     nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &UserActions{
				sessionStore: tt.fields.sessionStore,
				userStore:    tt.fields.userStore,
				hasher:       tt.fields.hasher,
				log:          tt.fields.log,
			}
			_, err := a.Login(tt.args.ctx, tt.args.req)
			if err == nil && tt.wantErr {
				t.Errorf("Login() error = nil, wanted %v", err)
				return
			}
			if err != nil && !tt.wantErr {
				t.Errorf("Login() error = %v, wanted nil", err)
				return
			}
			if err != tt.err {
				t.Errorf("Login() error = %v, wanted %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestUserActions_Logout(t *testing.T) {
	logger, err := log.NewLogger(log.WithName("user_actions"))
	if err != nil {
		t.Errorf("error creating logger: %s", err)
		return
	}

	type fields struct {
		sessionStore store.Session
		userStore    store.User
		hasher       hash.Hasher
		log          *zap.SugaredLogger
	}
	type args struct {
		ctx context.Context
		req *LogoutRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    LogoutResponse
		wantErr bool
		err     error
	}{
		{
			name: "user id is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LogoutRequest{},
			},
			want:    LogoutResponse{},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("User ID is a required field")),
		},
		{
			name: "session not found does not return error",
			fields: fields{
				sessionStore: storeMock.SessionStore{Err: store.ErrNotFound{Obj: "session"}},
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LogoutRequest{UserID: "123"},
			},
			want:    LogoutResponse{},
			wantErr: false,
			err:     nil,
		},
		{
			name: "db error returns internal error",
			fields: fields{
				sessionStore: storeMock.SessionStore{Err: goerrs.New("something happened in the db")},
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &LogoutRequest{UserID: "123"},
			},
			want:    LogoutResponse{},
			wantErr: true,
			err:     errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error deleting session: %s", "something happened in the db"))),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &UserActions{
				sessionStore: tt.fields.sessionStore,
				userStore:    tt.fields.userStore,
				hasher:       tt.fields.hasher,
				log:          tt.fields.log,
			}
			_, err := a.Logout(tt.args.ctx, tt.args.req)
			if err == nil && tt.wantErr {
				t.Errorf("Login() error = nil, wanted %v", err)
				return
			}
			if err != nil && !tt.wantErr {
				t.Errorf("Login() error = %v, wanted nil", err)
				return
			}
			if err != tt.err {
				t.Errorf("Login() error = %v, wanted %v", err, tt.err)
				return
			}
		})
	}
}

func TestUserActions_SignUp(t *testing.T) {
	logger, err := log.NewLogger(log.WithName("user_actions"))
	if err != nil {
		t.Errorf("error creating logger: %s", err)
		return
	}

	type fields struct {
		sessionStore store.Session
		userStore    store.User
		hasher       hash.Hasher
		log          *zap.SugaredLogger
	}
	type args struct {
		ctx context.Context
		req *SignUpRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    SignUpResponse
		wantErr bool
		err     error
	}{
		{
			name: "name is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &SignUpRequest{
					Name:     "",
					Username: "david",
					Password: "password123",
				},
			},
			want:    SignUpResponse{},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Name is a required field")),
		},
		{
			name: "username is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &SignUpRequest{
					Name:     "David",
					Username: "",
					Password: "password123",
				},
			},
			want:    SignUpResponse{},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Username is a required field")),
		},
		{
			name: "password is required",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &SignUpRequest{
					Name:     "David",
					Username: "david",
					Password: "",
				},
			},
			want:    SignUpResponse{},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Password is a required field")),
		},
		{
			name: "password must be at least 11 characters",
			fields: fields{
				sessionStore: nil,
				userStore:    nil,
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &SignUpRequest{
					Name:     "David",
					Username: "david",
					Password: "password12",
				},
			},
			want:    SignUpResponse{},
			wantErr: true,
			err:     errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage("Password must be at least 11 characters")),
		},
		{
			name: "db error returns internal error",
			fields: fields{
				sessionStore: nil,
				userStore:    storeMock.UserStore{Err: goerrs.New("something happened in the db")},
				hasher:       nil,
				log:          logger,
			},
			args: args{
				ctx: nil,
				req: &SignUpRequest{
					Name:     "David",
					Username: "david",
					Password: "password123",
				},
			},
			want:    SignUpResponse{},
			wantErr: true,
			err:     errors.NewInternalError(errors.IDDatabase, errors.WithMessage("error creating user: something happened in the db")),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &UserActions{
				sessionStore: tt.fields.sessionStore,
				userStore:    tt.fields.userStore,
				hasher:       tt.fields.hasher,
				log:          tt.fields.log,
			}
			_, err := a.SignUp(tt.args.ctx, tt.args.req)
			if err == nil && tt.wantErr {
				t.Errorf("Login() error = nil, wanted %v", err)
				return
			}
			if err != nil && !tt.wantErr {
				t.Errorf("Login() error = %v, wanted nil", err)
				return
			}
			if err != tt.err {
				t.Errorf("Login() error = %v, wanted %v", err, tt.err)
				return
			}
		})
	}
}
