package actions

import (
	"context"
	goerrs "errors"
	"fmt"

	"github.com/djacobs24/rest-api/errors"
	"github.com/djacobs24/rest-api/model"
	"github.com/djacobs24/rest-api/pkg/hash"
	"github.com/djacobs24/rest-api/pkg/log"
	"github.com/djacobs24/rest-api/store"
	"github.com/djacobs24/validate"
	"go.uber.org/zap"
)

// NewUserActions constructs a new UserActions.
func NewUserActions(sessionStore store.Session, userStore store.User, hasher hash.Hasher) (*UserActions, error) {
	logger, err := log.NewLogger(log.WithName("user_actions"))
	if err != nil {
		return nil, fmt.Errorf("error creating logger: %s", err)
	}

	return &UserActions{
		sessionStore: sessionStore,
		userStore:    userStore,
		hasher:       hasher,
		log:          logger,
	}, nil
}

// UserActions holds the dependencies for all user actions.
type UserActions struct {
	sessionStore store.Session
	userStore    store.User
	hasher       hash.Hasher
	log          *zap.SugaredLogger
}

//==================================================
// Login
//==================================================

// LoginRequest is the request to log a user into the app.
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate validates the LoginRequest.
func (r *LoginRequest) Validate() error {
	return validate.New(
		validate.StringExists("Username", r.Username),
		validate.StringExists("Password", r.Password),
	)
}

// LoginResponse is the response to a user's attempt to log into the app.
type LoginResponse struct {
	SessionToken string
	SessionTTL   int64
	UserID       string
}

// Login logs a user into the app.
func (a *UserActions) Login(ctx context.Context, req *LoginRequest) (LoginResponse, error) {
	resp := LoginResponse{}

	if err := req.Validate(); err != nil {
		a.log.Errorf("error validating login request: %s", err)
		return resp, errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage(err.Error()))
	}

	user, err := a.userStore.FindByUsername(ctx, req.Username)
	if err != nil {
		if goerrs.As(err, &store.ErrNotFound{}) {
			a.log.Errorf("user with username '%s' not found", req.Username)
			return resp, errors.NewUnauthenticatedError(errors.IDInvalidCredentials)
		}

		a.log.Errorf("error finding user with username %s: %s", req.Username, err)
		return resp, errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error finding user with username %s: %s", req.Username, err)))
	}

	if err := a.hasher.Compare(user.PasswordHash(), req.Password); err != nil {
		a.log.Errorf("user with username '%s' entered an incorrect password", req.Username)
		return resp, errors.NewUnauthenticatedError(errors.IDInvalidCredentials)
	}

	sess := model.NewSession(user.ID())
	if err := a.sessionStore.Create(ctx, sess); err != nil {
		a.log.Errorf("error creating user: %s", err)
		return resp, errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error creating user: %s", err)))
	}

	resp.SessionToken = sess.Token()
	resp.SessionTTL = sess.ExpiresAt()
	resp.UserID = user.ID()

	return resp, nil
}

//==================================================
// Logout
//==================================================

// LogoutRequest is the request to log a user out of the app.
type LogoutRequest struct {
	UserID string
}

// Validate validates the LogoutRequest.
func (r *LogoutRequest) Validate() error {
	return validate.New(
		validate.StringExists("User ID", r.UserID),
	)
}

// LogoutResponse is the response to a user logging out of the app.
type LogoutResponse struct{}

// Logout logs a user out of the app.
func (a *UserActions) Logout(ctx context.Context, req *LogoutRequest) (LogoutResponse, error) {
	resp := LogoutResponse{}

	if err := req.Validate(); err != nil {
		a.log.Errorf("error validating logout request: %s", err)
		return resp, errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage(err.Error()))
	}

	if err := a.sessionStore.DeleteByUserID(ctx, req.UserID); err != nil {
		if goerrs.As(err, &store.ErrNotFound{}) {
			// A session doesn't exist for this user
			return resp, nil
		}

		a.log.Errorf("error deleting session: %s", err)
		return resp, errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error deleting session: %s", err)))
	}

	return resp, nil
}

//==================================================
// SignUp
//==================================================

// SignUpRequest is the request to sign up a new user.
type SignUpRequest struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate validates the SignUpRequest.
func (r *SignUpRequest) Validate() error {
	return validate.New(
		validate.StringExists("Name", r.Name),
		validate.StringExists("Username", r.Username),
		validate.StringExists("Password", r.Password),
		validate.StringLenGreaterThan("Password", r.Password, 10),
	)
}

// SignUpResponse is the response to signing up a new user.
type SignUpResponse struct {
	User model.User
}

// SignUp signs up a new user.
func (a *UserActions) SignUp(ctx context.Context, req *SignUpRequest) (SignUpResponse, error) {
	resp := SignUpResponse{}

	if err := req.Validate(); err != nil {
		a.log.Errorf("error validating signup request: %s", err)
		return resp, errors.NewInvalidArgumentError(errors.IDInvalidRequest, errors.WithPrettyMessage(err.Error()))
	}

	user := model.NewUser(req.Name, req.Username, req.Password)

	if err := a.userStore.Create(ctx, user); err != nil {
		a.log.Errorf("error creating user: %s", err)
		return resp, errors.NewInternalError(errors.IDDatabase, errors.WithMessage(fmt.Sprintf("error creating user: %s", err)))
	}

	resp.User = user

	return resp, nil
}
