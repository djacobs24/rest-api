package model

import (
	"encoding/json"
	"time"
)

// NewUser creates a new User.
func NewUser(name, username, password string) User {
	return User{
		name:      name,
		username:  username,
		password:  password,
		locked:    false,
		lastLogin: 0,
		createdAt: time.Now().Unix(),
		updatedAt: 0,
		deletedAt: 0,
	}
}

// User represents a user in the app.
type User struct {
	id        string
	name      string
	username  string
	password  string
	locked    bool
	lastLogin int64
	createdAt int64
	updatedAt int64
	deletedAt int64
}

// ID returns the user's id.
func (u *User) ID() string {
	return u.id
}

// PasswordHash returns the hashed password.
func (u *User) PasswordHash() string {
	return u.password
}

// Marshal marshals the user for the response.
func (u *User) Marshal() []byte {
	b, _ := json.Marshal(struct {
		ID       string `json:"id"`
		Name     string `json:"name"`
		Username string `json:"username"`
	}{
		ID:       u.id,
		Name:     u.name,
		Username: u.username,
	})

	return b
}
