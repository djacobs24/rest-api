package model

import (
	"encoding/json"
	"math"
)

const (
	defaultPagination = true
	defaultPage       = 1
	defaultPerPage    = 20
)

// NewMeta constructs a new Meta.
func NewMeta(options ...MetaOption) Meta {
	req := &Meta{
		paginate: defaultPagination,
	}

	for _, o := range options {
		o(req)
	}

	if req.paginate {
		if req.currentPage <= 0 {
			req.currentPage = defaultPage
		}
		if req.itemsPerPage <= 0 {
			req.itemsPerPage = defaultPerPage
		}
	}

	return *req
}

// MetaOption provides a way to pass options to a meta request.
type MetaOption func(req *Meta)

// WithCurrentPage is a meta option that adds the current page to the request.
func WithCurrentPage(currentPage int) MetaOption {
	return func(req *Meta) {
		if currentPage > 0 {
			req.currentPage = currentPage
		}
	}
}

// WithItemsPerPage is a meta option that adds the items per page to the request.
func WithItemsPerPage(perPage int) MetaOption {
	return func(req *Meta) {
		req.itemsPerPage = perPage
	}
}

// WithSearchTerm is a meta option that adds a search term to the request.
func WithSearchTerm(term string) MetaOption {
	return func(req *Meta) {
		req.searchTerm = term
	}
}

// WithSortBy is a meta option that adds a sort to the request.
func WithSortBy(sort []string) MetaOption {
	return func(req *Meta) {
		req.sortBy = sort
	}
}

// WithoutPagination is a meta option that will turn pagination off.
func WithoutPagination() MetaOption {
	return func(req *Meta) {
		req.paginate = false
	}
}

// Meta holds the metadata for queries.
type Meta struct {
	paginate     bool
	currentPage  int
	totalPages   int
	itemsPerPage int
	totalItems   int
	sortBy       []string
	searchTerm   string
}

// Paginate returns true if the request needs pagination.
func (m Meta) Paginate() bool {
	return m.paginate
}

// CurrentPage returns the page being requested.
func (m Meta) CurrentPage() int {
	return m.currentPage
}

// TotalPages returns the total number of pages found.
func (m Meta) TotalPages() int {
	return m.totalPages
}

// ItemsPerPage returns the number of items to return per page.
func (m Meta) ItemsPerPage() int {
	return m.itemsPerPage
}

// TotalItems returns the total number of items found.
func (m Meta) TotalItems() int {
	return m.totalItems
}

// SortBy returns the fields to sort by.
func (m Meta) SortBy() []string {
	return m.sortBy
}

// SearchTerm returns the search term.
func (m Meta) SearchTerm() string {
	return m.searchTerm
}

// AddResults calculates the metadata results.
func (m Meta) AddResults(totalResults int) Meta {
	resp := Meta{
		// Filters
		sortBy:     m.sortBy,
		searchTerm: m.searchTerm,
		// Pages
		paginate:    m.paginate,
		currentPage: m.currentPage,
		totalPages:  0,
		// Items
		itemsPerPage: m.itemsPerPage,
		totalItems:   totalResults,
	}

	if !m.paginate {
		return resp
	}

	if totalResults > m.itemsPerPage && totalResults != 0 && m.itemsPerPage > 0 {
		resp.totalPages = int(math.Ceil(float64(totalResults) / float64(m.itemsPerPage)))
	}

	if totalResults < m.itemsPerPage || m.itemsPerPage <= 0 {
		resp.totalPages = 1
	}

	if totalResults == 0 {
		resp.totalPages = 0
	}

	return resp
}

// Marshal marshals the metadata for the response.
func (m Meta) Marshal() []byte {
	b, _ := json.Marshal(struct {
		Paginate     bool     `json:"paginate"`
		CurrentPage  int      `json:"current_page"`
		TotalPages   int      `json:"total_pages"`
		ItemsPerPage int      `json:"items_per_page"`
		TotalItems   int      `json:"total_items"`
		SortBy       []string `json:"sort_by"`
		SearchTerm   string   `json:"search_term"`
	}{
		Paginate:     m.paginate,
		CurrentPage:  m.currentPage,
		TotalPages:   m.totalPages,
		ItemsPerPage: m.itemsPerPage,
		TotalItems:   m.totalItems,
		SortBy:       m.sortBy,
		SearchTerm:   m.searchTerm,
	})

	return b
}
