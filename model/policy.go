package model

// NewPolicy constructs a new policy with overwrites
func NewPolicy(subject PolicySubject, resource PolicyResource, role PolicyRole, overwrites ...PolicyPermission) Policy {
	perms := map[PolicyAction]bool{}
	for _, perm := range role.Permissions() {
		perms[perm.Action] = perm.Allow
	}

	// Overwrites role permissions
	for _, overwrite := range overwrites {
		perms[overwrite.Action] = overwrite.Allow
	}

	var permissions []PolicyPermission
	for action, allow := range perms {
		p := PolicyPermission{
			Action: action,
			Allow:  allow,
		}

		permissions = append(permissions, p)
	}

	return Policy{
		subject:     subject,
		resource:    resource,
		role:        role,
		permissions: permissions,
	}
}

// Policy represents a policy for a subject on a resource.
type Policy struct {
	subject     PolicySubject
	resource    PolicyResource
	role        PolicyRole
	permissions []PolicyPermission
}

// Can returns true if a policy allows an action
func (p *Policy) Can(action PolicyAction) bool {
	for _, p := range p.permissions {
		if p.Action == action && p.Allow {
			return true
		}
	}

	return false
}

// PolicySubject represents a subject in a policy.
type PolicySubject struct {
	ID   string
	Type PolicySubjectType
}

// PolicySubjectType is a type of subject.
type PolicySubjectType string

const (
	UserSubject PolicySubjectType = "user"
)

// PolicyResource represents a resource in a policy.
type PolicyResource struct {
	ID   string
	Type PolicyResourceType
}

// PolicyResourceType is a type of resource.
type PolicyResourceType string

// PolicyRole represents a subject's role.
type PolicyRole string

// Permissions returns the permissions for a role.
func (r PolicyRole) Permissions() []PolicyPermission {
	return nil
}

// PolicyPermission shows whether an action is allowable.
type PolicyPermission struct {
	Action PolicyAction
	Allow  bool
}

// PolicyAction represents the action a subject can take on a resource.
type PolicyAction string

const (
	ViewAction   PolicyAction = "view"
	ModifyAction PolicyAction = "modify"
	DeleteAction PolicyAction = "delete"
)
