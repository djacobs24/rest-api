package model

import (
	"reflect"
	"testing"
)

func TestMeta_AddResults(t *testing.T) {
	type fields struct {
		paginate     bool
		currentPage  int
		totalPages   int
		itemsPerPage int
		totalItems   int
		sortBy       []string
		searchTerm   string
	}
	type args struct {
		totalResults int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   Meta
	}{
		{
			name: "no pagination, no results",
			fields: fields{
				paginate:     false,
				currentPage:  0,
				itemsPerPage: 0,
				sortBy:       nil,
				searchTerm:   "",
			},
			args: args{
				totalResults: 0,
			},
			want: Meta{
				paginate:     false,
				currentPage:  0,
				totalPages:   0,
				itemsPerPage: 0,
				totalItems:   0,
				sortBy:       nil,
				searchTerm:   "",
			},
		},
		{
			name: "pagination, no results",
			fields: fields{
				paginate:     true,
				currentPage:  1,
				itemsPerPage: 5,
				sortBy:       nil,
				searchTerm:   "",
			},
			args: args{
				totalResults: 0,
			},
			want: Meta{
				paginate:     true,
				currentPage:  1,
				totalPages:   0,
				itemsPerPage: 5,
				totalItems:   0,
				sortBy:       nil,
				searchTerm:   "",
			},
		},
		{
			name: "no pagination, 5 results",
			fields: fields{
				paginate:     false,
				currentPage:  0,
				itemsPerPage: 0,
				sortBy:       nil,
				searchTerm:   "",
			},
			args: args{
				totalResults: 5,
			},
			want: Meta{
				paginate:     false,
				currentPage:  0,
				totalPages:   0,
				itemsPerPage: 0,
				totalItems:   5,
				sortBy:       nil,
				searchTerm:   "",
			},
		},
		{
			name: "pagination, 5 results",
			fields: fields{
				paginate:     true,
				currentPage:  1,
				itemsPerPage: 3,
				sortBy:       nil,
				searchTerm:   "",
			},
			args: args{
				totalResults: 5,
			},
			want: Meta{
				paginate:     true,
				currentPage:  1,
				totalPages:   2,
				itemsPerPage: 3,
				totalItems:   5,
				sortBy:       nil,
				searchTerm:   "",
			},
		},
		{
			name: "non-results fields remain the same",
			fields: fields{
				paginate:     true,
				currentPage:  10,
				itemsPerPage: 10,
				sortBy:       []string{"name"},
				searchTerm:   "david",
			},
			args: args{
				totalResults: 100,
			},
			want: Meta{
				paginate:     true,
				currentPage:  10,
				totalPages:   10,
				itemsPerPage: 10,
				totalItems:   100,
				sortBy:       []string{"name"},
				searchTerm:   "david",
			},
		},
		{
			name: "num results less than per page",
			fields: fields{
				paginate:     true,
				currentPage:  1,
				itemsPerPage: 20,
				sortBy:       []string{"name"},
				searchTerm:   "david",
			},
			args: args{
				totalResults: 3,
			},
			want: Meta{
				paginate:     true,
				currentPage:  1,
				totalPages:   1,
				itemsPerPage: 20,
				totalItems:   3,
				sortBy:       []string{"name"},
				searchTerm:   "david",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := Meta{
				paginate:     tt.fields.paginate,
				currentPage:  tt.fields.currentPage,
				totalPages:   tt.fields.totalPages,
				itemsPerPage: tt.fields.itemsPerPage,
				totalItems:   tt.fields.totalItems,
				sortBy:       tt.fields.sortBy,
				searchTerm:   tt.fields.searchTerm,
			}
			if got := m.AddResults(tt.args.totalResults); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AddResults() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewMeta(t *testing.T) {
	type args struct {
		options []MetaOption
	}
	tests := []struct {
		name string
		args args
		want Meta
	}{
		{
			name: "pagination options",
			args: args{[]MetaOption{WithCurrentPage(1), WithItemsPerPage(5), WithSearchTerm("david"), WithSortBy([]string{"name"})}},
			want: Meta{
				paginate:     true,
				currentPage:  1,
				itemsPerPage: 5,
				sortBy:       []string{"name"},
				searchTerm:   "david",
			},
		},
		{
			name: "no pagination",
			args: args{[]MetaOption{WithoutPagination()}},
			want: Meta{
				paginate:     false,
				currentPage:  0,
				itemsPerPage: 0,
				sortBy:       nil,
				searchTerm:   "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMeta(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMeta() = %v, want %v", got, tt.want)
			}
		})
	}
}
