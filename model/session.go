package model

import (
	"time"

	"github.com/djacobs24/rest-api/pkg/rand"
)

const (
	expiresInSecs = 24 * 60 * 60
	sessionLen    = 32
)

// NewSession creates a new Session.
func NewSession(subjectID string) Session {
	return Session{
		token:     rand.AlphaNumericString(sessionLen),
		subjectID: subjectID,
		expiresAt: time.Now().Add(expiresInSecs * time.Second).Unix(),
	}
}

// Session represents a subject's session in the application.
type Session struct {
	token     string
	subjectID string
	expiresAt int64
}

// Token returns the session's token.
func (s *Session) Token() string {
	return s.token
}

// SubjectID returns the subject's id from the session.
func (s *Session) SubjectID() string {
	return s.subjectID
}

// ExpiresAt returns the Unix time when the session expires.
func (s *Session) ExpiresAt() int64 {
	return s.expiresAt
}

// IsExpired returns true if the session is expired.
func (s *Session) IsExpired() bool {
	if time.Now().Unix() >= s.expiresAt {
		return true
	}

	return false
}
