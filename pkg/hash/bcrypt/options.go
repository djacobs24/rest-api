package bcrypt

// Option gives our client optional arguments.
type Option func(c *Client)

// WithCost adds a custom cost to the hash.
func WithCost(cost int) Option {
	return func(c *Client) {
		c.cost = cost
	}
}
