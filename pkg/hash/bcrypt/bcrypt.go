package bcrypt

import (
	"golang.org/x/crypto/bcrypt"
)

const (
	defaultCost = 11
)

// NewClient constructs a new Client.
func NewClient(options ...Option) *Client {
	c := &Client{
		cost: defaultCost,
	}

	for _, option := range options {
		option(c)
	}

	return c
}

// Client holds the dependencies needed to generate
// and compare hashes using bcrypt.
type Client struct {
	cost int
}

// GenerateHash generates a hash from a password using bcrypt.
func (c *Client) GenerateHash(plain string) (string, error) {
	h, err := bcrypt.GenerateFromPassword([]byte(plain), c.cost)
	if err != nil {
		return "", err
	}

	return string(h), nil
}

// Compare compares a hash and a password using bcrypt.
func (c *Client) Compare(hash, plain string) error {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(plain))
}
