package mock

// Hasher mocks hash.Hasher.
type Hasher struct {
	Hash string
	Err  error
}

// GenerateHash ...
func (h Hasher) GenerateHash(plain string) (string, error) {
	return h.Hash, h.Err
}

// Compare ...
func (h Hasher) Compare(hash, plain string) error {
	return h.Err
}
