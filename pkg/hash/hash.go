package hash

// Hasher provides a way to generate and compare hashes.
type Hasher interface {
	GenerateHash(plain string) (string, error)
	Compare(hash, plain string) error
}
