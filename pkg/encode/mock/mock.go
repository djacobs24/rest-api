package mock

// Encoder mocks encode.Encoder.
type Encoder struct {
	Encoded string
	Decoded string
	Err     error
}

// Encode ...
func (e Encoder) Encode(plain string) string {
	return e.Encoded
}

// Decode ...
func (e Encoder) Decode(encoded string) (string, error) {
	return e.Decoded, e.Err
}
