package base64

import (
	"encoding/base64"
)

// NewClient constructs a new Client.
func NewClient() *Client {
	return &Client{}
}

// Client holds the dependencies needed to encode and
// decode strings with base64 encoding.
type Client struct{}

// Encode encodes a string with base64.
func (c *Client) Encode(plain string) string {
	return base64.StdEncoding.EncodeToString([]byte(plain))
}

// Decode decodes a base64 encoded string.
func (c *Client) Decode(encoded string) (string, error) {
	b, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
