package encode

// Encoder provides a way to encode and decode strings.
type Encoder interface {
	Encode(plain string) string
	Decode(encoded string) (string, error)
}
