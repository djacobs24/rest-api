// Package env provides helpers for getting environment variables.
package env

import (
	"fmt"
	"os"
	"strconv"
)

// LookupWithDefault gets a value from the environment or returns the
// default if the lookup fails.
func LookupWithDefault(name, def string) string {
	val, ok := os.LookupEnv(name)
	if !ok {
		return def
	}

	return val
}

// LookupIntWithDefault gets a value from the environment and converts it
// to an int, or returns the default if not provided or conversion fails.
func LookupIntWithDefault(name string, def int) int {
	val, ok := os.LookupEnv(name)
	if !ok {
		return def
	}

	i, err := strconv.Atoi(val)
	if err != nil {
		return def
	}

	return i
}

// LookupBoolWithDefault gets a value from the environment and converts it
// to a boolean, or returns the default if not provided.
func LookupBoolWithDefault(name string, def bool) bool {
	val, ok := os.LookupEnv(name)
	if !ok {
		return def
	}

	return val == "true"
}

// LookupOrErr gets a value from the environment or returns an error.
func LookupOrErr(name string) (string, error) {
	val, ok := os.LookupEnv(name)
	if !ok {
		return "", fmt.Errorf("%s was not provided", name)
	}

	return val, nil
}

// LookupIntOrErr gets a value from the environment and converts it
// to an int, or returns an error if not provided or conversion fails.
func LookupIntOrErr(name string) (int, error) {
	val, err := LookupOrErr(name)
	if err != nil {
		return 0, err
	}

	i, err := strconv.Atoi(val)
	if err != nil {
		return 0, err
	}

	return i, nil
}
