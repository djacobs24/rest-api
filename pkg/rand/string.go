package rand

import (
	"math/rand"
	"time"
)

const (
	alphaLowerCharSet        = "abcdefghijklmnopqrstuvwxyz"
	alphaUpperCharSet        = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	numericCharSet           = "0123456789"
	alphaCharSet             = alphaLowerCharSet + alphaUpperCharSet
	alphaLowerNumericCharSet = alphaLowerCharSet + numericCharSet
	alphaNumericCharSet      = alphaLowerCharSet + alphaUpperCharSet + numericCharSet
)

var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

// AlphaString returns a random alpha string of the provided length.
func AlphaString(length int) string {
	return stringWithCharset(length, alphaCharSet)
}

// AlphaNumericString returns a random alphanumeric string of the
// provided length.
func AlphaNumericString(length int) string {
	return stringWithCharset(length, alphaNumericCharSet)
}

// AlphaLowerNumericString returns a random lowercase alphanumeric
// string of the provided length.
func AlphaLowerNumericString(length int) string {
	return stringWithCharset(length, alphaLowerNumericCharSet)
}

func stringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}

	return string(b)
}
