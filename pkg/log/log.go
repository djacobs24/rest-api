package log

import (
	"fmt"

	"github.com/djacobs24/rest-api/pkg/env"
	"go.uber.org/zap"
)

const (
	devEnv  = "dev"
	prodEnv = "prod"
)

// Option provides a way to add optional arguments to the logger.
type Option func(logger *zap.Logger)

// WithName adds a name to the logger.
func WithName(name string) Option {
	return func(logger *zap.Logger) {
		logger.Named(name)
	}
}

// NewLogger constructs a new logger.
func NewLogger(options ...Option) (*zap.SugaredLogger, error) {
	var err error
	var logger *zap.Logger

	switch env.LookupWithDefault("ENVIRONMENT", devEnv) {
	case prodEnv:
		logger, err = zap.NewProduction()
	default:
		logger, err = zap.NewDevelopment()
	}
	if err != nil {
		return nil, fmt.Errorf("error creating a logger: %w", err)
	}

	for _, option := range options {
		option(logger)
	}

	logger.WithOptions()

	return logger.Sugar(), nil
}
